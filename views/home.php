<?php
/**
 * Article.php - renders the main page
 * 
 * @author Bugslayer
 * 
 */
?>
<div class="jumbotron">
	<h1>Welkom op onze site</h1>
	<table class="table table-striped">
  <tr>
    <th>Browser Type</th>
    <th>Statstieken</th> 
  </tr>
   <tr>
    <td>IE (<=11)</td>
    <td>40,6%</td> 
  </tr>
  <tr>
    <td><b>Chrome</b></td>
    <td>24,2%</td> 
  </tr>
  <tr>
    <td>Safari</td>
    <td>20,6%</td> 
  </tr>
  <tr>
    <td>Firefox</td>
    <td>10,4%</td> 
  </tr>
  <tr>
    <td><em>Android</em></td>
    <td>3,0%</td>
  </tr>
  <tr>
    <td>Opera</td>
    <td>0,6%</td> 
  </tr>
  <tr>
    <td>Overig(inc. mobiel)</td>
    <td>0,6%</td> 
  </tr>
 </table>
 <h2>Ons Hoofdkantoor</h2>
 <img src="head-office.png">
 
  
</div>