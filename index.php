<?php
/**
 * Index.php - main page of the application. All requests should land here, except for ajax calls
 * 
 * @author Bugslayer
 * 
 */
// Session is reuired for authorisation
session_start ();

// Process global query vars or set default values
$action = 'show';
if (isset ( $_GET ['action'] )) {
	$action = $_GET ['action'];
}

$page = 'home';
if (isset ( $_GET ['page'] )) {
	$page = $_GET ['page'];
}

// Process other actions than show
if ($action != 'show') {
	// Try to load a controller script
	$controller_filename = 'controllers/' . $page . '.php';
	if (file_exists ( $controller_filename )) {
		include $controller_filename;
		exit ( 0 );
	} else {
		// If no controller script exists...
		die ( "400 - Bad request" );
	}
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
		Remove this if you use the .htaccess -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Casustoets Webdeveloper">
<meta name="author" content="Bugslayer">

<title>Fa Reijnders</title>

<meta charset="utf-8">

<link rel="shortcut icon" href="favicon.ico">

<link rel="stylesheet" type="text/css"
	href="lib/bootstrap/css/bootstrap.css" />

<!-- javascripts laden -->
<script src="lib/jquery-2.0.3.min.js"></script>
<script src="lib/bootstrap/js/bootstrap.js"></script>
<script src="js/scripts.js"></script>
<script type="text/javascript"
	src="lib/jquery-validate/jquery.validate.js"></script>
<script type="text/javascript"
	src="lib/jquery-validate/additional-methods.js"></script>
<script type="text/javascript"
	src="lib/jquery-validate/localization/messages_nl.js"></script>
</head>

<body style="padding-top: 70px;">
	<div class="container">
		<nav class="navbar  navbar-inverse navbar-fixed-top" role="navigation">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">Reijnders</a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse"
					id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<?php include('components/account.php'); ?>
					</ul>
					<ul class="nav navbar-nav">
						<?php include('components/menu.php'); ?>
					</ul>
				</div>
			</div>
		</nav>
		<main>
			<?php
			$view_filename = 'views/' . $page . '.php';
			if (file_exists ( $view_filename ))
				include $view_filename;
			else
				include ('404.php');
			?>
		</main>

		<footer>
			<p>&copy; Copyright Fa Reijnders by Bugslayer</p>
		</footer>
	</div>

</body>
</html>

